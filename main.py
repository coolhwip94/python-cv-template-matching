
# Python program to illustrate
# template matching
import cv2
import numpy as np
from statistics import mean
from glob import glob


def get_template_coords(template_image, main_image, threshold=0.8):

    found_image = None
    # Read the main image
    img_rgb = cv2.imread(main_image)

    # Convert it to grayscale
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

    # Read the template
    template = cv2.imread(template_image, 0)

    # Perform match operations.
    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)

    # Store the coordinates of matched area in a numpy array
    loc = np.where(res >= threshold)

    if len(loc[0]) > 0 and len(loc[1]) > 0:

        # averagex = mean(loc[1])
        # averagey = mean(loc[0])

        # use first index for now
        averagex = loc[1][0]
        averagey = loc[0][0]

        found_image = (averagex, averagey)
    return found_image


def draw_template_findings(main_image, template_image, coords):

    main = cv2.imread(main_image)
    template = cv2.imread(template_image, 0)

    w, h = template.shape[::-1]
    cv2.rectangle(main, coords, (coords[0] + w, coords[1] + h), (0, 0, 255), 5)

    draw_filename = template_image.split('/')[-1].replace('.png', '')
    cv2.imwrite(f'detected_{draw_filename}.png', main)


def main():
    '''
    '''
    main_image = 'main_image.png'
    template_image = 'template.png'

    for template_image in glob('template_images/*.png'):
        coords = get_template_coords(template_image, main_image)

        if coords:
            draw_template_findings(main_image, template_image, coords)


if __name__ == "__main__":
    main()
