# Python CV Template Matching
> Simple project to showcase using python open cv to detect if an image exists within another image.


## Setup
---
- `main_image.png` : this is the main image that is being queried against. (script checks if template exists within this image)

- `template_images` : the images that will be checked if they exist or not within the main_image

- python :

```
# setup python virtual env

python3 -m venv venv

# activate

source venv/bin/activate

# install requirements

pip3 install -r requirements.txt
```

## Running
---
> This is mainly for proof of concept, so the results will be printed
```
python3 main.py
```

> If the image is found, it will also draw it on the original image and output as a new file.